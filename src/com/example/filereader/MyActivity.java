package com.example.filereader;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.example.filereader.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class MyActivity extends Activity {

	private final String PATH_TO_SD_CARD = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
	private StringBuilder sb;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		final EditText filenameEditText = (EditText) findViewById(R.id.filenameEditText);
		final EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
		final EditText fileContentEditText = (EditText) findViewById(R.id.fileContentEditText);

		final Button openButton = (Button) findViewById(R.id.openButton);
		final Button searchButton = (Button) findViewById(R.id.searchButton);
		final Button saveButton = (Button) findViewById(R.id.saveButton);

		openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                try {
                    sb = readAndDisplayFile(PATH_TO_SD_CARD + filenameEditText.getText());
                    fileContentEditText.append(sb.toString());
                } catch (IOException e) {
                    Toast.makeText(MyActivity.this, "Cannot open file", Toast.LENGTH_SHORT).show();
                }
            }
        });

		filenameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView textView, final int i, final KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    try {
                        sb = readAndDisplayFile(PATH_TO_SD_CARD + filenameEditText.getText());
                        fileContentEditText.append(sb.toString());
                    } catch (IOException e) {
                        Toast.makeText(MyActivity.this, "Cannot open file", Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(filenameEditText.getWindowToken());
                    return true;
                }
                return false;
            }
        });

		searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String searchString = searchEditText.getText().toString();
                if (!"".equals(searchString)) {
                    int searchStartPosition = getSearchStartPosition(
                            searchString,
                            sb,
                            fileContentEditText.getSelectionStart(),
                            fileContentEditText.getSelectionEnd()
                    );
                    int startIndex = sb.indexOf(searchString, searchStartPosition);
                    if (startIndex != -1) {
                        fileContentEditText.setSelection(startIndex, startIndex + searchString.length());
                        fileContentEditText.requestFocus();
                    } else {
                        Toast.makeText(MyActivity.this, "No results", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

		searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(final TextView textView, final int i, final KeyEvent keyEvent) {
				String searchString = searchEditText.getText().toString();
				if (i == EditorInfo.IME_ACTION_DONE && !"".equals(searchString)) {
					int searchStartPosition = getSearchStartPosition(
							searchString,
							sb,
							fileContentEditText.getSelectionStart(),
							fileContentEditText.getSelectionEnd()
					);
					int startIndex = sb.indexOf(searchString, searchStartPosition);
					if (startIndex != -1) {
						fileContentEditText.setSelection(startIndex, startIndex + searchString.length());
						fileContentEditText.requestFocus();
					} else {
						Toast.makeText(MyActivity.this, "No results", Toast.LENGTH_SHORT).show();
					}
					hideKeyboard(searchEditText.getWindowToken());
					return true;
				}
				return false;
			}
		});

		saveButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View view) {
				sb.setLength(0);
				sb.append(fileContentEditText.getText());
				try {
					saveNewFileContents(sb, PATH_TO_SD_CARD + filenameEditText.getText());
				} catch (IOException e) {
					Toast.makeText(MyActivity.this, "Cannot save file", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void hideKeyboard(IBinder binder) {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(binder, 0);
	}

	private void saveNewFileContents(final StringBuilder sb, String filename) throws IOException {
		final FileWriter fw = new FileWriter(new File(filename));
		try {
			fw.write(sb.toString());
			Toast.makeText(this, "File is saved", Toast.LENGTH_SHORT).show();
		} finally {
			fw.close();
		}
	}

	private static int getSearchStartPosition(
			final String searchString,
			final StringBuilder sb,
			final int currentSelectionStart,
			final int currentSelectionEnd
	) {
		int result = 0;
		if (currentSelectionStart != currentSelectionEnd
				&& searchString.equals(sb.substring(currentSelectionStart, currentSelectionEnd))) {
			result = currentSelectionEnd + 1;
		}
		return result;
	}

	private static StringBuilder readAndDisplayFile(final String filename) throws IOException {
		final StringBuilder res = new StringBuilder();
		final BufferedReader br = new BufferedReader(new FileReader(new File(filename)));
		String s;
		try {
			while ((s = br.readLine()) != null) {
				res.append(s).append("\n");
			}
		} finally {
			br.close();
		}
		return res;
	}

}
